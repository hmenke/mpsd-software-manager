"""mpsd-software: tool for installation of software as on MPSD HPC."""
import datetime
import importlib.metadata
import sys
from pathlib import Path

__version__ = importlib.metadata.version("mpsd_software_manager")


config_vars = {
    #  kept inside the mpsd_release folder
    "cmd_log_file": "mpsd-software.log",
    #  Metadata tags
    "metadata_tag_open": "!<meta>",
    "metadata_tag_close": "</meta>!",
    "spack_environments_repo": "https://gitlab.gwdg.de/mpsd-cs/spack-environments.git",
    "init_file": ".mpsd-software-root",
}

command_name = Path(sys.argv[0]).name

call_date_iso = (
    datetime.datetime.now().replace(microsecond=0).isoformat().replace(":", "-")
)
