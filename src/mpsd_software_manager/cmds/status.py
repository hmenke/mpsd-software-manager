"""Functions to query the status of an mpsd-software installation."""
import logging
import os
from pathlib import Path
from typing import List, Union

from mpsd_software_manager import config_vars
from mpsd_software_manager.utils.microarch import get_native_microarchitecture
from mpsd_software_manager.utils.pretty_print_spec import pretty_print_spec
from mpsd_software_manager.utils.run import run


def environment_status(
    mpsd_release: str, root_dir: Path, package_set="NONE"
) -> Union[dict, List[str], None]:
    """Show status of release in installation.

    - 1) If no mpsd_release, list available releases
    - 2) If mpsd_release, list available toolchains
    - 3) If mpsd_release and toolchain, list available packages

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    root_dir : pathlib.Path
        A Path object pointing to the root directory of the installation.
        Expect a subfolder root/mpsd_release in which we search for the
        toolchains.
    package_set : str, optional
        A string representing the package_sets to show the status for.

    Returns
    -------
    installed_release : List[str]
        A list of installed (valid) releases.

    OR
    toolchain_map : dict
        A dictionary containing available microarchitectures as keys and
        a list of available package_sets as values for each microarchitecture.
        If the release is not installed/found, None is returned.

        Note: only toolchains can be reported at the moment (i.e. package_sets
        such as global and global_generic are missing, even if installed).

    OR
    package_list : List[str]
        A list of strings representing the packages installed for the
        specified package_sets and microarch.

    """
    msg = f"Showing status of release {mpsd_release} in {root_dir}"
    logging.info(msg)
    if not mpsd_release:
        # 1) if no mpsd_release is specified, list available releases
        return list_installed_releases(root_dir=root_dir, print_output=True)
    # 2) if mpsd_release is specified, list installed toolchains
    # Test is the mpsd_release is valid
    if mpsd_release not in list_installed_releases(root_dir=root_dir):
        logging.error(f"MPSD release '{mpsd_release}' is not available.")
        return None
    if package_set == "NONE":
        return list_installed_toolchains(
            mpsd_release=mpsd_release, root_dir=root_dir, print_output=True
        )
    # 3) if mpsd_release and toolchain is specified, list installed packages
    # check that the package-set is a valid toolchain
    if (
        package_set
        not in list_installed_toolchains(mpsd_release=mpsd_release, root_dir=root_dir)[
            get_native_microarchitecture()
        ]
    ):
        logging.error(f"Package-set '{package_set}' is not available.")
        return None
    return list_installed_packages(
        mpsd_release=mpsd_release,
        root_dir=root_dir,
        package_set=package_set,
        microarch=get_native_microarchitecture(),
    )


def list_installed_releases(root_dir: Path, print_output: bool = False) -> List[str]:
    """
    List installed releases.

    Parameters
    ----------
    root_dir : pathlib.Path

    Returns
    -------
    installed_releases : list
        A list of strings representing the installed releases.
    """
    plog = logging.getLogger("print")
    list_of_files = os.listdir(root_dir)
    installed_releases = [
        x for x in list_of_files if (root_dir / x / "spack-environments").exists()
    ]
    if print_output:
        plog.info("Installed MPSD software releases:")
        for release in installed_releases:
            plog.info(f"    {release}")
    return installed_releases


def list_installed_toolchains(
    mpsd_release: str, root_dir: Path, print_output: bool = False
) -> Union[dict, None]:
    """
    List installed toolchains.

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    root_dir : pathlib.Path
        A Path object pointing to the root directory of the installation.
        Expect a subfolder root/mpsd_release in which we search for the
        toolchains.
    print_output : bool, optional
        A boolean indicating whether to print the output to the terminal.

    Returns
    -------
    toolchain_map : dict
        A dictionary containing available microarchitectures as keys and
        a list of available package_sets as values for each microarchitecture.
        If the release is not installed/found, None is returned.

        Note: only toolchains can be reported at the moment (i.e. package_sets
        such as global and global_generic are missing, even if installed).
    """
    plog = logging.getLogger("print")
    release_base_dir = root_dir / mpsd_release
    microarch = get_native_microarchitecture()
    toolchain_dir = release_base_dir / microarch
    spack_dir = toolchain_dir / "spack"

    # if the mpsd_release does not exist:
    if not release_base_dir.exists():
        logging.debug(f"Directory {str(release_base_dir)} does not exist.")
        logging.error(f"MPSD release '{mpsd_release}' is not installed.")
        return None

    # if the mpds_release directory exists but the spack repository is not fully
    # cloned - indicates some kind of incomplete installation:
    if not spack_dir.exists():
        logging.info(f"Could not find directory {spack_dir}.")
        logging.error(
            f"MPSD release '{mpsd_release}' has not been completely installed."
        )

        return None

    # find all folders for all microarch in the release directory
    # except for the blacklisted files
    black_listed_files = [
        config_vars["cmd_log_file"],
        "spack-environments",
        "logs",
        "mpsd-spack-cache",
    ]

    list_of_microarchs_candidates = os.listdir(release_base_dir)
    list_of_microarchs = [
        x for x in list_of_microarchs_candidates if x not in black_listed_files
    ]
    logging.debug(f"{list_of_microarchs=}")

    toolchain_map = {}
    for microarch in list_of_microarchs:
        # get a list of all the toolchains in the microarch
        possible_toolchains = (release_base_dir / microarch).glob(
            "lmod/Core/toolchains/*.lua"
        )
        # append toolchain which is the name of the file without the .lua extension
        toolchain_map[microarch] = [toolchain.stem for toolchain in possible_toolchains]

    logging.debug(f"{toolchain_map=}")

    # pretty print the toolchain map key as the heading
    # and the value as the list of toolchains
    if print_output:
        plog.info(f"Installed toolchains ({mpsd_release}):\n")
        for microarch, toolchains in toolchain_map.items():
            plog.info(f"- {microarch}")
            for toolchain in toolchains:
                plog.info(f"    {toolchain}")
            plog.info(
                f"    [module use {str(release_base_dir / microarch / 'lmod/Core')}]"
            )
            plog.info("")
    return toolchain_map


def list_installed_packages(
    mpsd_release: str, root_dir: Path, package_set: str, microarch: str
) -> Union[List[str], None]:
    """
    List installed packages and their specs.

    Uses `spack -e package_set find` to list the installed packages,
    in the following format
    "{name}{@versions}{%compiler.name}{@compiler.versions}{compiler_flags}{variants}{arch=architecture}"

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    root_dir : pathlib.Path
        A Path object pointing to the root directory of the installation.
        Expect a subfolder root/mpsd_release in which we search for the
        toolchains.
    package_set : str
        A string representing the package_sets to show the packages for.
    microarch : str
        A string representing the microarchitecture to show the packages for.

    Returns
    -------
    list
        A list of strings representing the packages installed for the
        specified package_sets and microarch.
        If the release is not installed/found, None is returned.

    """
    plog = logging.getLogger("print")
    plog.info(f"listing packages installed for {package_set=}, {microarch=}")
    spack_dir = root_dir / mpsd_release / microarch / "spack"
    spack_env = spack_dir / "share" / "spack" / "setup-env.sh"
    commands_to_execute = [
        f"export SPACK_ROOT={spack_dir}",  # need to set SPACK_ROOT in dash and sh
        f". {spack_env}",
        f"spack -e {package_set}"
        " find --format "
        r"{name}{@versions}{%compiler.name}{@compiler.versions}{compiler_flags}{variants}{arch=architecture}",
    ]
    process = run(
        " && ".join(commands_to_execute), shell=True, check=True, capture_output=True
    )
    package_list = process.stdout.decode().strip().split("\n")
    for package in package_list:
        pretty_print_spec(package)
    return package_list
