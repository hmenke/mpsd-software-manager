import os
import subprocess

import pytest

from mpsd_software_manager.utils.filesystem_utils import os_chdir
from mpsd_software_manager.utils.run import run


def test_run_method(tmp_path):
    """Run tests for run method."""
    # test a command with options:
    assert run(["date", "+%Y-%m-%d"]).returncode == 0
    assert run("date +%Y-%m-%d", shell=True).returncode == 0

    # tests interacting with the file system
    with os_chdir(str(tmp_path)):
        # ensure single string command works
        assert run(("ls -l"), shell=True).returncode == 0
        # test spaces are handled correctly:
        assert run(["touch", "file1", "file2"]).returncode == 0
        assert os.path.exists("file1")
        assert os.path.exists("file2")
        # test output is captured:
        assert (
            b"Hello, world!\n"
            in run(["echo", "Hello, world!"], capture_output=True).stdout
        )

    # check exceptions
    with pytest.raises(FileNotFoundError):
        run(["doesnotexistcommand"])

    # check error code is checked
    # 1. expect this to parse: return code is non-zero, but we don't check
    run(["ls", "/doesnotexist"]),
    # 2. expect this to fail:
    with pytest.raises(subprocess.CalledProcessError):
        run(["ls", "/doesnotexist"], check=True)
