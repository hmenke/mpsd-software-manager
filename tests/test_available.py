from mpsd_software_manager.cmds import available


def test_get_available_releases():
    res = available.get_available_releases()
    assert "dev-23a" in res
    assert len(res) >= 1
    for release in res:
        assert isinstance(release, str)


def test_get_available_package_sets():
    """
    Test that available package_sets are reported correctly.

    Needs internet access to succeed.
    """
    package_sets = available.get_available_package_sets("dev-23a")
    assert sorted(package_sets) == sorted(
        [
            "foss2021a-cuda-mpi",
            "foss2021a-mpi",
            "foss2021a-serial",
            "foss2022a-cuda-mpi",
            "foss2022a-mpi",
            "foss2022a-serial",
            "global",
            "global_generic",
        ]
    )
