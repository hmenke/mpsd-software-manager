"""Tests for mpsd-software-environment.py."""

import sys
from pathlib import Path

import pytest

from mpsd_software_manager import mpsd_software


def test_argument_parsing_logic(mocker):
    """Test to find errors in argparse logic.

    Strategy:

    In each of the tests below, we are setting the sys.argv to simulate the
    input from the command line, and in each instance, we ensure that the
    mocked function get the arguments as expected. The function is mocked not
    to carry out any activity.

    """
    # pretend we have a rootdir defined
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.get_root_dir", return_value=Path(".")
    )

    sys.argv = ["mpsd-software-tests", "init"]
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.initialise_environment", return_value=None
    )
    with pytest.raises(SystemExit):
        mpsd_software.main()
    call_argument = mock.call_args[0][0]
    assert isinstance(call_argument, Path)

    ### available
    sys.argv = ["mpsd-software-tests", "available"]
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.get_available_releases", return_value=None
    )
    with pytest.raises(SystemExit):
        mpsd_software.main()

    sys.argv = ["mpsd-software-tests", "available", "dev-23a"]
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.get_available_package_sets",
        return_value=None,
    )
    mpsd_software.main()
    call_argument = mock.call_args[0][0]
    assert call_argument == "dev-23a"

    ### prepare
    sys.argv = ["mpsd-software-tests", "prepare", "dev-23a"]
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.prepare_environment", return_value=None
    )
    mpsd_software.main()
    call_argument = mock.call_args[0][0]
    assert call_argument == "dev-23a"

    ### install
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.install_environment", return_value=None
    )
    sys.argv = ["mpsd-software-tests", "install", "dev-23a", "foss2022a-mpi"]
    mpsd_software.main()
    assert mock.call_args[0][0] == "dev-23a"
    assert mock.call_args[0][1] == ["foss2022a-mpi"]

    sys.argv = [
        "mpsd-software-tests",
        "install",
        "23b",
        "foss2022a-mpi",
        "foss2022a-serial",
    ]
    mpsd_software.main()
    assert mock.call_args[0][0] == "23b"
    assert mock.call_args[0][1] == ["foss2022a-mpi", "foss2022a-serial"]

    ### status
    mock = mocker.patch(
        "mpsd_software_manager.mpsd_software.environment_status", return_value=None
    )
    sys.argv = ["mpsd-software-tests", "status", "dev-23a"]
    mpsd_software.main()
    assert mock.call_args[0][0] == "dev-23a"

    ### remove (argparse doesn't allow this yet.
    ### Copy from 'install' when the time has come.)


def test_interface(tmp_path):
    """Test other things (not implemented yet)."""
    pass
    # ensure that installing without package_sets only passes the available package_sets
    # check that the script branch and hash are correct when running the script
    # check that the help message is printed when no arguments are provided
    # check that the help message is printed when -h is provided
    # check that the error messages are also logged to the log file
    # check that `/` in release is handled correctly
    # check that the cmd_log file contains sys arguments
    # check that the cmd_log file contains the script version for init
    # check that the cmd_log file contains the location of APEX log


# other tests to add (ideally)
# - get_native_microarchitecture()
